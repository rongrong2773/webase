git提交地址

https://git.oschina.net/emf102/baseweb.git

环境搭建：

1、eclipse J2EE版 下载地址
	http://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/kepler/R/eclipse-jee-kepler-R-win32-x86_64.zip
   下载完成后解压到文件

2、maven安装
	 （1）下载地址http://maven.apache.org/download.cgi
	 （2）maven3.1解压到 D:\maven
	 （3）添加环境变量MAVEN_HOME = D:\maven\maven3.1 在path下添加%MAVEN_HOME%\bin注意用;分隔
	 （4）在D:\maven 建立仓库目录repo
	 （5）把maven3.1\conf\settings.xml拷贝到C:\Users\Administrator\.m2目录下
	 （6）修改settings.xml文件内容:<localRepository>d:/maven/repo</localRepository>,指定maven的仓库目录	
	 (7) 执行mvn help:system

3、eclipse插件安装
   (已经集成了maven和git,暂时不需要)
   
4、ext4编辑器搭建
	（1）下载地址 http://www.spket.com/  包括jsb文件  ext4 下载地址：http://www.sencha.com/products/extjs/download/ext-js-4.2.1/2281
	（2）安装 解压压缩包  将features和plugins 覆盖到eclipse里对应的文件夹
	（3）重启eclipse 
	（4）window->Preferences->spket->JavaScript Profiles -> New (Ext4 ) 
	（5）AddLibrary 选择extjs
	（6）AddFile  将下载的jsb文件放到 ext4目录下 ，选择此文件 ->OK
	（7）把spket编辑器设置为默认
5、关闭eclipse js验证：
	将项目中的.project文件中的
	org.eclipse.wst.jsdt.core.javascriptValidator
	org.eclipse.wst.jsdt.core.jsNature
	两行注释掉即可。
   
6、快捷键配置
	(1) ctrl+4 commit
	(2) ctrl+5 push
	(3) ctrl+\ add too GitIndex