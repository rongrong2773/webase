package me.webase.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import me.webase.util.json.JSONArray;
import me.webase.util.json.JSONException;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.jdbc.Work;
import org.hibernate.persister.entity.AbstractEntityPersister;

public class BaseDao implements IDao {
	
	@Resource
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	public Transaction openTransaction(){
		return getSession().beginTransaction();
	}
	
	@Override
	public Object add(Object object) {
		 getSession().save(object);
		 return object;
	}

	@Override
	public void delete(Object object) {
		getSession().delete(object);
	}

	@Override
	public void deleteById(Class<?> clazz,Serializable id) {
		delete(find(clazz,id));
	}
	
	@Override
	public int deleteByIds(Class<?> clazz,Object...ids){
		DeleteByIdWrok work = new DeleteByIdWrok(clazz, ids);
		getSession().doWork(work);
		return work.updateCount;
	}
	

	@Override
	public Object update(Object object) {
		this.getSession().saveOrUpdate(object);
		return object;
	}
	
	/**
	 * 更新符合条件的数据为空
	 * 
	 * @param mapValue
	 * @param criterions
	 * @return
	 */
	public int updateNull(Class<?> clazz,String whereSql,String...properties) {
		StringBuilder sb = new StringBuilder("update " + clazz.getName()+" set ");
		for(String str:properties){
			sb.append(str+"= null,");
		}
		sb.deleteCharAt(sb.length()-1);
		if(null!=whereSql){
			sb.append(" where " + whereSql);
		}
		return getSession().createQuery(sb.toString()).executeUpdate();
	}

	@Override
	public long getCount(Class<?> clazz) {
		return (long) createCriteria(clazz).setProjection(Projections.rowCount())
				.list().get(0);
	}

	@Override
	public long getCount(Class<?> clazz,String property, Object value) {
		return (long) createCriteria(clazz).add(Restrictions.eq(property, value)).setProjection(Projections.rowCount())
				.list().get(0);
	}

	@Override
	public long getCount(Class<?> clazz,Map<String, Object> condition) {
		return (long) createEqualCriteria(clazz, condition).setProjection(Projections.rowCount())
				.list().get(0);
	}

	@Override
	public long getCount(Class<?> clazz,Criterion... criterions) {
		
		return (long)addCriterions(createCriteria(clazz),criterions).setProjection(Projections.rowCount())
				.list().get(0);
	}

	@Override
	public long getCount(Class<?> clazz,String whereHql) {
		String s = "select count(*) from " + clazz.getName() + " where "
				+ whereHql;
		Query query = getSession().createQuery(s);
		return (long) query.uniqueResult();
	
	}

	@Override
	public Object find(Class<?> clazz,Serializable id) {
		return getSession().get(clazz, id);
	}

	@Override
	public Object findOnlyOne(Class<?> clazz,String property, Object value) {
		return createCriteria(clazz).add(Restrictions.eq(property, value)).uniqueResult();
	}

	@Override
	public Object findOnlyOne(Class<?> clazz, Criterion... criterions) {
		Criteria criteria = this.getSession().createCriteria(clazz);
		for(Criterion criterion:criterions){
			criteria.add(criterion);
		}
		return criteria.uniqueResult();
	}

	@Override
	public Object findOnlyOne(Class<?> clazz, Map<String, Object> conditions) {
		return createEqualCriteria(clazz, conditions).uniqueResult();
	}
	
	public Object findOnlyOne(Class<?> clazz,String wherehql){
		String s = "from " + clazz.getName() + " " +wherehql;
		return this.createQuery(s).uniqueResult();
	}

	@Override
	public List<?> queryAll(Class<?> clazz) {
		return createCriteria(clazz).list();
	}

	@Override
	public List<?> queryByCondition(Class<?> clazz, String property,
			Object value) {
		return createCriteria(clazz).add(Restrictions.eq(property, value)).list();
	}

	@Override
	public List<?> queryByCondition(Class<?> clazz,
			Map<String, Object> conditions, String orderBy, boolean isAsc) {
		return createEqualCriteria(clazz, conditions).addOrder(isAsc ? Order.asc(orderBy) : Order.desc(orderBy)).list();
	}

	@Override
	public List<?> queryByCondition(Class<?> clazz, Criterion... criterions) {
		Criteria criteria = this.getSession().createCriteria(clazz);
		for(Criterion criterion:criterions){
			criteria.add(criterion);
		}
		return criteria.list();
	}

	@Override
	public List<?> queryByWhereCondition(Class<?> clazz, String whereHql) {
		return getSession().createQuery(addWhere(createSelectQueryStr(clazz), whereHql)).list();
	}

	@Override
	public Page pageQuery(Class<?> clazz, int pageNo, int pageSize) {
		return pageQuery(clazz, pageNo, pageSize, true, null);
	}

	@Override
	public Page pageQuery(Class<?> clazz, int pageNo, int pageSize,
			boolean isAsc, String orderBy) {
		return pageQuery(clazz, pageNo, pageSize,null, isAsc, orderBy);
	}

	@Override
	public Page pageQuery(Class<?> clazz, int pageNo, int pageSize,Map<String, Object> conditions,
			 boolean isAsc, String orderBy) {
		int start = Page.getStartOfPage(pageNo, pageSize);
		Criteria criteria = null;
		long count;
		if (null != conditions) {
			criteria = createEqualCriteria(clazz, conditions);
			count = getCount(clazz,conditions);
		} else {
			count = getCount(clazz);
			criteria = createCriteria(clazz);
		}
		
		if (null != orderBy) {
			addOrderBy(criteria, orderBy, isAsc);
		}
		List<?> l = criteria.setMaxResults(pageSize).setFirstResult(start).list();
		return new Page(start, count, pageSize, l);
	}

	@Override
	public Page pageQuery(Class<?> clazz, int pageNo, int pageSize,
			boolean isAsc, String orderBy, Criterion... criterions) {
		int start = Page.getStartOfPage(pageNo, pageSize);
		Criteria criteria = createCriteria(clazz);
		long count;
		if (null != criterions) {
			addCriterions(criteria, criterions);
			count = getCount(clazz,criterions);
		} else {
			count = getCount(clazz);
		}
		if (null != orderBy) {
			addOrderBy(criteria, orderBy, isAsc);
		}
		List<?> l = criteria.setMaxResults(pageSize).setFirstResult(start).list();
		return new Page(start, count, pageSize, l);
	}

	@Override
	public Page pageQuery(Class<?> clazz,String whereHql,int pageNo, int pageSize,
			boolean isAsc, String orderBy) {

		long count;
		int start = Page.getStartOfPage(pageNo, pageSize);
		String hql = createSelectQueryStr(clazz);
		if (null != whereHql) {
			hql = addWhere(hql, whereHql);
			count = getCount(clazz,whereHql);
		} else {
			count = getCount(clazz);
		}
		if (null != orderBy) {
			hql = addOrder(hql, orderBy, isAsc);
		}
		
		List<?> l = getSession().createQuery(hql).setMaxResults(pageSize)
				.setFirstResult(start).list();
		return new Page(start, count, pageSize, l);
	}
	
	/**
	 * 利用json组装的查询条件进行查询
	 * @param clazz 实体类
	 * @param jsonCondition  json where查询条件
	 * @param pageNo 页码
	 * @param pageSize 每页条数
	 * @param isAsc 是否升序
	 * @param orderBy 排序字段
	 * @return
	 */
	public Page pageQueryByJson(Class<?> clazz,String jsonCondition,int pageNo, int pageSize,
			boolean isAsc, String orderBy) {
		FormatHql hql = new FormatHql(clazz,getSession(),jsonCondition);
		long count = hql.getCount();
		int start = Page.getStartOfPage(pageNo, pageSize);
		List<?> l = hql.createQuery().setMaxResults(pageSize).setFirstResult(start).list();
		return new Page(start, count, pageSize, l);
	}
	
	/**
	 * json 条件样式
	 * 	[{property:'name',condition:' is null '},
	 *  {property:'name',condition:' is not null '},
	 *  {property:'name',condition:' = ', singleValue:'zhou'},
	 *  {property:'age',condition:' in ',listValue:[16,17,18]},
	 *  {property:'age',condition:' not in ',listValue:[16,17,18]},
	 *  {property:'age',condition:' between ',betweenValue:[18,23]},
	 *  {property:'age',condition:' not between ',betweenValue:[18,23]}]
	 * @param jsonCondition
	 */
	private String parseJsonConditon(String jsonCondition){
		JSONArray jArr;
		try {
			jArr = new JSONArray(jsonCondition);
			if(jArr.length() == 0){
				return null;
			}
			
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * 创建带有相等条件限制的
	 * @param clazz
	 * @param map
	 * @return
	 */
	private Criteria createEqualCriteria(Class<?> clazz,Map<String,Object> map){
		Criteria criteria = getSession().createCriteria(clazz);
		for(Map.Entry<String,Object> entry:map.entrySet()){
			criteria.add(Restrictions.eq(entry.getKey(), entry.getValue()));
		}
		return criteria;
	}
	
	private String createEqString(Map<String,Object> map){
		StringBuilder sb = null;
		for(Map.Entry<String,Object> entry:map.entrySet()){
			if(null == sb){
				sb = new StringBuilder(" where " + entry.getKey()+"=:"+entry.getKey());
			} else {
				sb.append(" and " + entry.getKey()+"=:"+entry.getKey());
			}
		}
		return sb.toString();
	}
	
	private String createDelHqlStr(Class<?> clazz){
		return "DELETE FROM "+clazz.getName() +" "; 
	}
	
	/**
	 * 创建普通Criteria对象
	 * 
	 * @return
	 */
	private Criteria createCriteria(Class<?> clazz) {
		return getSession().createCriteria(clazz);
	}
	
	/**
	 * 创建不带条件的字符串
	 * @return
	 */
	private String createSelectQueryStr(Class<?> clazz) {
		return "from " + clazz.getName();
	}
	
	/**
	 * 创建带where条件的字符串
	 * @param hql
	 * @param whereSql
	 * @return
	 */
	private String addWhere(String hql, String whereSql) {
		return hql + " where " + whereSql;
	}
	
	/**
	 * 创建带where条件和排序的字符串
	 * @param hql
	 * @param orderBy
	 * @param isAsc
	 * @return
	 */
	private String addOrder(String hql, String orderBy, boolean isAsc) {
		return hql + " order by " + orderBy + (isAsc ? " asc" : " desc");
	}
	/**
	 * 创建Criteria对象，带有查询条件
	 * 
	 * @param <T>
	 * @param entityClass
	 * @param criterions
	 * @return
	 */
	private Criteria addCriterions(Criteria criteria, Criterion... criterions) {
		if (null != criterions) {
			for (Criterion c : criterions) {
				criteria.add(c);
			}
		}
		return criteria;
	}
	
	/**
	 * 创建Criteria对象，带有查询条件，有排序功能。
	 * 
	 * @param <T>
	 * @param entityClass
	 * @param orderBy
	 * @param isAsc
	 * @param criterions
	 * @return
	 */
	private Criteria addOrderBy(Criteria criteria, String orderBy, boolean isAsc) {
		if (null != orderBy) {
			criteria.addOrder(isAsc ? Order.asc(orderBy) : Order.desc(orderBy));
		}
		return criteria;
	}
	
	private Query createQuery(String hql){
		return this.getSession().createQuery(hql);
	}
	
	/**
	 * 通过JDBC删除
	 * @author Administrator
	 *
	 */
	private class DeleteByIdWrok implements Work{
		
		AbstractEntityPersister classMetadata;
		String sql;
		Object[] ids;
		public int updateCount;
		
		public DeleteByIdWrok(Class clazz,Object[] ids){
			this.classMetadata = (AbstractEntityPersister)sessionFactory.getClassMetadata(clazz);
			this.ids = ids;
			StringBuilder sb = new StringBuilder();
			for(int i=0,j=ids.length;i<j;i++){
				sb.append("?");
				if(i+1 < j){
					sb.append(",");
				}
			}
			
			sql = "DELETE FROM "+classMetadata.getTableName()
					+ " WHERE " + classMetadata.getIdentifierColumnNames()[0]+" IN ("+sb.toString()+")" ;
		}
		
		@Override
		public void execute(Connection connection) throws SQLException {
			PreparedStatement stmt = connection.prepareStatement(sql);
			for(int i=1,j=ids.length;i<=j;i++){
				stmt.setObject(i, ids[i-1]);
			}
			
			updateCount = stmt.executeUpdate();
		}
		
		
		
	}
}
