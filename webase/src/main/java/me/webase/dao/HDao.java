package me.webase.dao;

import java.io.Serializable;
import java.util.List;


public interface HDao {


	/**
	 * 插入一条数据
	 * @param object
	 * @return
	 */
	public Object add(Object object);
	
	/**
	 * 删除一条数据
	 * @param object
	 * @return
	 */
	public void delete(Object object);
	
	/**
	 * 根据ID删除一条数据
	 * @param id
	 * @return
	 */
	public void deleteById(Class<?> clazz,Serializable id);
	
	/**
	 * 根据多个ID删除多条数据
	 * @param id
	 * @return
	 */
	public int deleteByIds(Class<?> clazz,Object...ids);

	
	/**
	 * 更新一条数据
	 * @param object
	 * @return
	 */
	public Object update(Object object);
	
	/**
	 * 取得总记录数
	 * @return
	 */
	public long getCount(Class<?> clazz);

	/**
	 * 取得总数通过 hql条件，不包含where
	 * @param whereSql
	 * @return
	 */
	public long getCount(Class<?> clazz,String whereHql);
	
	/**
	 * 根据ID查询一条记录
	 * @param id
	 * @return
	 */
	public Object find(Class<?> clazz,Serializable id);
	
	
	/**
	 * 根据条件查询一个记录
	 * @param clazz
	 * @param whereSql
	 * @return
	 */
	public Object findOnlyOne(Class<?> clazz,String whereSql);
	
	/**
	 * 查询全部记录
	 * @return
	 */
	public List<?> queryAll(Class<?> clazz);
	
	
	/**
	 * 分页查询
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public Page pageQuery(Class<?> clazz,int pageNo, int pageSize);

	/**
	 * 带排序的分页查询
	 * @param pageNo
	 * @param pageSize
	 * @param isAsc
	 * @param orderBy
	 * @return
	 */
	public Page pageQuery(Class<?> clazz,int pageNo, int pageSize, boolean isAsc,
			String orderBy);

	
	/**
	 * 根据HQL分页查询
	 * @param pageNo
	 * @param pageSize
	 * @param isAsc
	 * @param orderBy
	 * @param whereHql 注意where的写法 email = 'email1'
	 * @return
	 */
	public Page pageQuery(Class<?> clazz, String whereHql,int pageNo, int pageSize, boolean isAsc,
			String orderBy);

}
