package me.webase.action;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;

@Component
@Scope("prototype")
public class TestAction extends ActionSupport{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8308760811549459836L;

	public void showMessage(){
		System.out.println("调用Action");
	}
}
