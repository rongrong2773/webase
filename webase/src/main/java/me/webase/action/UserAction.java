package me.webase.action;

import me.webase.model.User;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

@Controller
@Scope("prototype")
public class UserAction extends AbstractBaseAction<User> {
	public UserAction() {
		System.out.println("UserAction创建");
	}
}
