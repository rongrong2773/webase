package me.webase.action;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import me.webase.util.JsonBuilder;
import me.webase.util.ModelUtil;
import me.webase.util.PropUtil;
import me.webase.util.ResponseUtil;

import org.apache.struts2.ServletActionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
/**
 * 用于获取model的域
 * @author Administrator
 *
 */
@Controller("modelAction")
@Scope("singleton")
public class ModelAction {
	//域的缓存容器
	private Map<String,String> map = new HashMap<String,String>();
	//默认包
	private String defaultPackage =PropUtil.get("sys.default.modelPackage");
	//用于判断是否使用简写包名
	private String packagePattern =PropUtil.get("sys.default.modelPackagePattern");
	
	public ModelAction() {
		System.out.println("ModelAction创建");
	}
	
	public void getModelFields(){
		String modelName = ServletActionContext.getRequest().getParameter("modelName");
		String excludes = ServletActionContext.getRequest().getParameter("excludes");
		if(excludes == null){
			excludes ="";
		}
		
		if(modelName == null || modelName.equals("")){
			
		}
		
		if(!modelName.startsWith(packagePattern)){
			modelName =defaultPackage+ modelName;
		}
		
		if(map.get(modelName) == null){
			Field[] fields =null;
			try {
				fields = ModelUtil.getClassFields(Class.forName(modelName), true);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String s = JsonBuilder.getModelFileds("user", fields, excludes);
			map.put(modelName, s);
			ResponseUtil.response(s);
		} else {
			ResponseUtil.response(map.get(modelName));
		}
	}
}
