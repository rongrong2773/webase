package me.webase.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import me.webase.dao.IDao;
import me.webase.dao.Page;

import org.hibernate.criterion.Criterion;

public class DefaultService implements Service {
	
	private IDao dao;
	
	public void setDao(IDao dao) {
		this.dao = dao;
	}
	
	@Override
	public Object add(Object object) {
		return dao.add(object);
	}

	@Override
	public void delete(Object object) {
		dao.delete(object);

	}

	@Override
	public void deleteById(Class<?> clazz, Serializable id) {
		dao.deleteById(clazz, id);
	}

	@Override
	public int deleteByIds(Class<?> clazz, Object... ids) {
		return dao.deleteByIds(clazz, ids);
	}

	@Override
	public Object update(Object object) {
		return dao.update(object);
	}

	@Override
	public long getCount(Class<?> clazz) {
		return dao.getCount(clazz);
	}

	@Override
	public long getCount(Class<?> clazz, String property, Object value) {
		return dao.getCount(clazz, property,value);
	}

	@Override
	public long getCount(Class<?> clazz, Map<String, Object> condition) {
		return dao.getCount(clazz, condition);
	}

	@Override
	public long getCount(Class<?> clazz, Criterion... criterions) {
		return dao.getCount(clazz, criterions);
	}

	@Override
	public long getCount(Class<?> clazz, String whereHql) {
		return dao.getCount(clazz, whereHql);
	}

	@Override
	public Object find(Class<?> clazz, Serializable id) {
		return dao.find(clazz, id);
	}

	@Override
	public Object findOnlyOne(Class<?> clazz, String property, Object value) {
		return dao.findOnlyOne(clazz, property, value);
	}

	@Override
	public Object findOnlyOne(Class<?> clazz, Criterion... criterions) {
		return dao.findOnlyOne(clazz, criterions);
	}

	@Override
	public Object findOnlyOne(Class<?> clazz, Map<String, Object> conditions) {
		return dao.findOnlyOne(clazz,conditions);
	}

	@Override
	public List<?> queryAll(Class<?> clazz) {
		return dao.queryAll(clazz);
	}

	@Override
	public List<?> queryByCondition(Class<?> clazz, String property,
			Object value) {
		return dao.queryByCondition(clazz, property, value);
	}

	@Override
	public List<?> queryByCondition(Class<?> clazz,
			Map<String, Object> conditions, String orderBy, boolean isAsc) {
		return dao.queryByCondition(clazz, conditions, orderBy, isAsc);
	}

	@Override
	public List<?> queryByCondition(Class<?> clazz, Criterion... criterions) {
		return dao.queryByCondition(clazz, criterions);
	}

	@Override
	public List<?> queryByWhereCondition(Class<?> clazz, String whereHql) {
		return dao.queryByWhereCondition(clazz, whereHql);
	}

	@Override
	public Page pageQuery(Class<?> clazz, int pageNo, int pageSize) {
		return dao.pageQuery(clazz, pageNo, pageSize);
	}

	@Override
	public Page pageQuery(Class<?> clazz, int pageNo, int pageSize,
			boolean isAsc, String orderBy) {
		return dao.pageQuery(clazz, pageNo, pageSize, isAsc, orderBy);
	}

	@Override
	public Page pageQuery(Class<?> clazz, int pageNo, int pageSize,
			Map<String, Object> conditions, boolean isAsc, String orderBy) {
		return dao.pageQuery(clazz, pageNo, pageSize, conditions, isAsc, orderBy);
	}

	@Override
	public Page pageQuery(Class<?> clazz, int pageNo, int pageSize,
			boolean isAsc, String orderBy, Criterion... criterions) {
		return dao.pageQuery(clazz, pageNo, pageSize, isAsc, orderBy, criterions);
	}

	@Override
	public Page pageQuery(Class<?> clazz, String whereHql, int pageNo,
			int pageSize, boolean isAsc, String orderBy) {
		return dao.pageQuery(clazz, whereHql, pageNo, pageSize, isAsc, orderBy);
	}

}
