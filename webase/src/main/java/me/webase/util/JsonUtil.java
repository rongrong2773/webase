package me.webase.util;

import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializerProvider;


public class JsonUtil {
	
	private static ObjectMapper mapper = new ObjectMapper();

	/**
	 * java对象转换成json
	 * 
	 * @param obj
	 * @return
	 * @throws IOException
	 */
	public static String bean2Json(Object obj) throws IOException {
		StringWriter sw = new StringWriter();
		JsonGenerator gen = new JsonFactory().createJsonGenerator(sw);
		mapper.writeValue(gen, obj);
		gen.close();
		return sw.toString();
	}

	/**
	 * json转换成java对象
	 * 
	 * @param jsonStr
	 * @param objClass
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public static <T> T json2Bean(String jsonStr, Class<T> objClass)
			throws JsonParseException, JsonMappingException, IOException {
		return mapper.readValue(jsonStr, objClass);
	}
	
	public static String class2ExtJsModel(Class<?> clazz){
		String[] filters = null;  
        // 判断该类中是否包含懒加载对象，如果包含懒加载对象，则排除在外。  
        if (clazz.isAnnotationPresent(JsonIgnoreProperties.class)) {  
            filters = clazz.getAnnotation(JsonIgnoreProperties.class).value();  
        }  
        Field[] fields = clazz.getDeclaredFields();  
  
        // 这里将里面的包名改成你所需的，或者改用传参的方式也是一个不错的方法！  
        String sb = "Ext.define('SystemSetup.model." + clazz.getSimpleName()  
                + "', {\r\n\textend: 'Ext.data.Model',\r\n\tfields:[";  
  
        fieldFor: for (Field field : fields) {  
            if (filters != null) {  
                for (String f : filters) {  
                    if (f.equals(field.getName())) {  
                        continue fieldFor;  
                    }  
                }  
            }  
            sb += "'" + field.getName() + "',";  
        }  
        sb = sb.substring(0, sb.length() - 1);  
        sb += "]\r\n});";  
        // 在这里可以输出为文件，也可以返回一个字符串.  
        return sb.toString();  
	}
	
	/**
	 * 日期类型
	 * @author Administrator
	 *
	 */
	public static class DateFormat extends JsonSerializer<Date> {
		@Override
		public void serialize(Date value, JsonGenerator jgen,
				SerializerProvider provider) throws IOException,
				JsonProcessingException {
			SimpleDateFormat formatter = new SimpleDateFormat(DateUtil.DEFAULT_DATE_FORMAT);
			String formattedDate = formatter.format(value);
			jgen.writeString(formattedDate);
		}
	}
	
	/**
	 * 日期时间
	 * @author Administrator
	 *
	 */
	public static class TimeFormat extends JsonSerializer<Date> {
		@Override
		public void serialize(Date value, JsonGenerator jgen,
				SerializerProvider provider) throws IOException,
				JsonProcessingException {
			SimpleDateFormat formatter = new SimpleDateFormat(DateUtil.DEFAULT_TIME_FORMAT);
			String formattedDate = formatter.format(value);
			jgen.writeString(formattedDate);
		}
	}
}
