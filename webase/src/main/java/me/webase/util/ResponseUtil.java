package me.webase.util;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

public class ResponseUtil {
	
	public static void response(String contents){
		HttpServletResponse response = ServletActionContext.getResponse();
		if(response!=null){
			response.setContentType("text/html;charset=UTF-8;");
			Writer writer=null;
			try {
				response.setCharacterEncoding("UTF-8");
				writer=response.getWriter();
				writer.write(contents);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				try {
					writer.flush();
					writer.close();
					response.flushBuffer();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	
	}
}
