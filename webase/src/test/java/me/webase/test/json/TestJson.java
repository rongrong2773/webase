package me.webase.test.json;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import me.webase.test.model.User;
import me.webase.util.JsonBuilder;
import me.webase.util.JsonUtil;
import me.webase.util.ModelUtil;

import org.junit.Before;
import org.junit.Test;

public class TestJson {
	
	private static List<User> list = new ArrayList<User>();
	static {
		for(int i=0;i<10;i++){
			User user = new User();
			user.setAge(i+"");
			user.setEmail("email" + i);
			user.setName("zhou"+i);
			user.setDate(new Date());
			list.add(user);
		}
	}

	@Before
	public void setUp() throws Exception {
	}

	//@Test
	public void test() {
		String s = null;
		try {
			 s = JsonUtil.bean2Json(list);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(s);
	}
	
	@Test
	public void testFields(){
		Field[] fields = ModelUtil.getClassFields(User.class, true);
		String s = JsonBuilder.getModelFileds("user", fields, "name");
		System.out.println(s);
	}
}
