package me.webase.test.dao;

import java.util.List;

import me.webase.dao.BaseDao;
import me.webase.test.model.User;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpringDao {

	private static BaseDao dao;

	static {
		ApplicationContext ac = new ClassPathXmlApplicationContext(
				"applicationContext.xml");
		dao = (BaseDao) ac.getBean("dao");
	}

	@Before
	public void setUp() throws Exception {

		openTransaction();
	}

	@After
	public void closeSession() {
		commitTransaction();
		dao.getSession().close();
	}

	private void openTransaction() {
		System.out.println("开启事务");
		dao.openTransaction();
	}

	private void commitTransaction() {
		System.out.println("提交事务");
		dao.getSession().getTransaction().commit();
	}

	@Test
	public void test() {
		List<User> list = (List<User>) dao.queryAll(User.class);
		for (User user : list) {
			System.out.println("ID:" + user.getId() + "NAME:" + user.getName()
					+ "AGE:" + user.getAge() + "EMAIL:" + user.getEmail());
		}
	}

}
