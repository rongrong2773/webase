package me.webase.test.dao;

import java.util.List;

import me.webase.service.Service;
import me.webase.test.model.User;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestServiceDao {

	private static Service service;

	static {
		ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
		service = (Service) ac.getBean("service");
	}

	@Test
	public void test() {
		List<User> list = (List<User>) service.queryAll(User.class);
		for (User user : list) {
			System.out.println("ID:" + user.getId() + "NAME:" + user.getName()
					+ "AGE:" + user.getAge() + "EMAIL:" + user.getEmail());
		}
	}
	
	public void find(){
		User user = (User)service.find(User.class, 46);
		System.out.println(user.getAge());
	}

}
