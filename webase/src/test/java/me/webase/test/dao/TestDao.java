package me.webase.test.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.webase.dao.BaseDao;
import me.webase.dao.Page;
import me.webase.test.model.User;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.hibernate.jdbc.Work;
import org.hibernate.persister.entity.AbstractEntityPersister;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestDao {
	
	private static SessionFactory sessionFactory;
	
	private static BaseDao dao;
	
	static {
		Configuration cfg = new Configuration().configure();
		ServiceRegistry sr = new ServiceRegistryBuilder().applySettings(cfg.getProperties()).buildServiceRegistry();
		sessionFactory = cfg.buildSessionFactory(sr);
		dao = new BaseDao();
		dao.setSessionFactory(sessionFactory);
	}
	
	@Before
	public void setUp() throws Exception {
		
		openTransaction();
	}
	
	@After
	public void closeSession(){
		
		commitTransaction();
		dao.getSession().close();
	}
	
	private void openTransaction(){
		System.out.println("开启事务");
		dao.openTransaction();
	}
	
	private void commitTransaction(){
		System.out.println("提交事务");
		dao.getSession().getTransaction().commit();
	}
	
	/**
	 * 统计数量
	 * @return 总数
	 */
	private long getCount(){
		return dao.getCount(User.class);
	}
	
	public void testAdd() {
		List<User> list = produceUser(5);
		for(User user:list){
			dao.add(user);
		}
	}
	
	public void queryAll(){
		testAdd();
		List<User> list = (List<User> )dao.queryAll(User.class);
		for(User user:list){
			System.out.println("ID:"+user.getId()+"NAME:"+user.getName()+"AGE:"+user.getAge()+"EMAIL:"+user.getEmail());
		}
	}
	
	
	public void queryByCondition(){
		List<User> list = (List<User>)dao.queryByCondition(User.class, "email","email1");
		for(User user:list){
			System.out.println("ID:"+user.getId()+"NAME:"+user.getName()+"AGE:"+user.getAge()+"EMAIL:"+user.getEmail());
		}
	}
	
	public void queryByCondition1(){
		List<User> list = (List<User>)dao.queryByCondition(User.class,Restrictions.eq("email","email0"));
		for(User user:list){
			System.out.println("ID:"+user.getId()+"NAME:"+user.getName()+"AGE:"+user.getAge()+"EMAIL:"+user.getEmail());
		}
	}
	
	public void queryByCondition2(){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("email","email0");
		List<User> list = (List<User>)dao.queryByCondition(User.class,map,"id",false);
		for(User user:list){
			System.out.println("ID:"+user.getId()+"NAME:"+user.getName()+"AGE:"+user.getAge()+"EMAIL:"+user.getEmail());
		}
	}
	
	public void queryByWhereCondition(){
		List<User> list = (List<User>)dao.queryByWhereCondition(User.class, "id < 10");
		for(User user:list){
			System.out.println("ID:"+user.getId()+"NAME:"+user.getName()+"AGE:"+user.getAge()+"EMAIL:"+user.getEmail());
		}
	}
	
	
	public void pageQuery(){
		Page page = dao.pageQuery(User.class, 2, 5);
		List<User> list = (List<User>)page.getResult();
		for(User user:list){
			System.out.println("ID:"+user.getId()+"NAME:"+user.getName()+"AGE:"+user.getAge()+"EMAIL:"+user.getEmail());
		}
	}
	
	
	public void pageQuery1(){
		Page page = dao.pageQuery(User.class, 2, 5,true,"email");
		List<User> list = (List<User>)page.getResult();
		for(User user:list){
			System.out.println("ID:"+user.getId()+"NAME:"+user.getName()+"AGE:"+user.getAge()+"EMAIL:"+user.getEmail());
		}
	}
	
	
	public void pageQuery2(){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("email","email1");
		Page page = dao.pageQuery(User.class, 1, 5,map,true,"email");
		List<User> list = (List<User>)page.getResult();
		for(User user:list){
			System.out.println("ID:"+user.getId()+"NAME:"+user.getName()+"AGE:"+user.getAge()+"EMAIL:"+user.getEmail());
		}
	}

	public void pageQuery3(){
		Page page = dao.pageQuery(User.class, 3, 10, true, "id", Restrictions.lt("id", 27));
		List<User> list = (List<User>)page.getResult();
		for(User user:list){
			System.out.println("ID:"+user.getId()+"NAME:"+user.getName()+"AGE:"+user.getAge()+"EMAIL:"+user.getEmail());
		}
	}
	
	public void pageQuery4(){
		Page page = dao.pageQuery(User.class, "email = 'email1'", 3, 10, true, "id");
		List<User> list = (List<User>)page.getResult();
		for(User user:list){
			System.out.println("ID:"+user.getId()+"NAME:"+user.getName()+"AGE:"+user.getAge()+"EMAIL:"+user.getEmail());
		}
	}
	public void update(){
		User user = new User();
		user.setId(1);
		user.setEmail("zhouqiang1002");
		user.setName("zhouqiang");
		user.setAge("28");
		dao.update(user);
	}
	
	
	public void update2(){
		User user = new User();
		user.setId(1);
		user.setEmail("zhouqiang1002");
		user.setName("zhouqiang");
		user.setAge("28");
		dao.updateNull(User.class, "id<5", "email");
	}
	
	
	public void delete(){
		User user = new User();
		user.setId(1);
		dao.delete(user);
	}
	
	public void delete2(){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("email","'email1'");
		//dao.deleteByCondition(User.class, map);
	}
	

	public void delete4(){
		dao.deleteById(User.class, 4);
	}
	
	
	public void delete5(){
		//dao.deleteByIds(User.class, "id", "'2'","'3'","'4'");
		AbstractEntityPersister classMetadata = (AbstractEntityPersister)sessionFactory.getClassMetadata(User.class);
		String tableName = classMetadata.getTableName();
		String[] pkPropertyName = classMetadata.getPropertyColumnNames("email");
		System.out.println(pkPropertyName[0]);
		Work work = new Work(){
			@Override
			public void execute(Connection connection) throws SQLException {
				PreparedStatement stmt=connection.prepareStatement("");
			}
			
		};
	}
	@Test
	public void delete6(){
		String[] ids = {"7","8","9"};
		System.out.println("删除条数："+dao.deleteByIds(User.class,ids));
	}
	

	/**
	 * 创建User
	 * @param count 需要创建的数量
	 * @return
	 */
	private List<User> produceUser(int count){
		List<User> list = new ArrayList();
		for(int i=0;i<count;i++){
			User user = new User();
			user.setAge("27"+i);
			user.setName("zhou"+i);
			user.setEmail("email"+i);
			list.add(user);
		}
		return list;
	}

}
