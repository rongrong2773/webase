package me.webase.test.dao;

import junit.framework.TestCase;
import me.webase.dao.BaseDao;
import me.webase.test.model.User;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class TestSessionFactroy extends TestCase {
	
	private SessionFactory sessionFactory;
	
	public void testFactroy() {
		Configuration cfg = new Configuration().configure();
		ServiceRegistry sr = new ServiceRegistryBuilder().applySettings(cfg.getProperties()).buildServiceRegistry();
		sessionFactory = cfg.buildSessionFactory(sr);
	}
	
	public void testDaoAdd(){
		testFactroy();
		User user = new User();
		user.setAge("27");
		user.setName("zhou");
		user.setEmail("email");
		BaseDao dao = new BaseDao();
		dao.setSessionFactory(sessionFactory);
		Transaction t= dao.getSession().beginTransaction();
		dao.add(user);
		t.commit();
	}
	
	
	
}
