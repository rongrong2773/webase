package me.webase.test.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import me.webase.dao.BaseDao;
import me.webase.model.User;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestInitData {
	
private static SessionFactory sessionFactory;
	
	private static BaseDao dao;
	
	static {
		Configuration cfg = new Configuration().configure();
		ServiceRegistry sr = new ServiceRegistryBuilder().applySettings(cfg.getProperties()).buildServiceRegistry();
		sessionFactory = cfg.buildSessionFactory(sr);
		dao = new BaseDao();
		dao.setSessionFactory(sessionFactory);
	}
	
	@Before
	public void setUp() throws Exception {
		openTransaction();
	}
	
	@After
	public void closeSession(){
		
		commitTransaction();
		dao.getSession().close();
	}
	
	private void openTransaction() {
		System.out.println("开启事务");
		dao.openTransaction();
	}

	private void commitTransaction() {
		System.out.println("提交事务");
		dao.getSession().getTransaction().commit();
	}

	@Test
	public void initData() {
		List<User> list = produceData(1000);
		for(User user:list){
			dao.add(user);
		}
	}
	
	private List<User> produceData(int count){
		List<User>  list = new ArrayList<User>();
		for(int i=0;i<count;i++){
			User user = new User();
			user.setName("zhou_"+i);
			user.setEmail("zhou_"+i+"@126.com");
			user.setAge((int)(Math.random()*120));;
			long time = new Date().getTime()-(long)(Math.random()*1000*60*60*24*365);
			
			user.setAddDate(new Date(time));
			list.add(user);
		}
		return list;
		
	}

}
