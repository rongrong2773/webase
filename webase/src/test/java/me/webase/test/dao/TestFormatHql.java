package me.webase.test.dao;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import me.webase.dao.BaseDao;
import me.webase.dao.FormatHql;
import me.webase.model.User;

import org.apache.commons.beanutils.PropertyUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestFormatHql {

	private static SessionFactory sessionFactory;

	private static BaseDao dao;

	static {
		Configuration cfg = new Configuration().configure();
		ServiceRegistry sr = new ServiceRegistryBuilder().applySettings(
				cfg.getProperties()).buildServiceRegistry();
		sessionFactory = cfg.buildSessionFactory(sr);
		dao = new BaseDao();
		dao.setSessionFactory(sessionFactory);
	}

	@Before
	public void setUp() throws Exception {

		openTransaction();
	}

	@After
	public void closeSession() {

		commitTransaction();
		dao.getSession().close();
	}

	private void openTransaction() {
		System.out.println("开启事务");
		dao.openTransaction();
	}

	private void commitTransaction() {
		System.out.println("提交事务");
		dao.getSession().getTransaction().commit();
	}

	//@Test
	public void testFormatHql() {
		Session session = sessionFactory.getCurrentSession();
		String s = "[["
				+ "{property:'name',condition:' is not null '},"
				+ "{property:'name',condition:' like ', singleValue:'%zhou%'},"
				+ "{property:'age',condition:' in ',listValue:[16,17,18]}]]";
			//	+ "{property:'age',condition:' not in ',listValue:[16,17,18]},"
			//	+ "{property:'age',condition:' between ',betweenValue:[18,23]},"
			//	+ "{property:'age',condition:' not between ',betweenValue:[18,23]}]]";
		List<User> list = (List<User>)new FormatHql(User.class,session,s).createQuery().list();
		for(User user:list){
			System.out.println(user.getAge());
		}
	}
	
	//@Test
	public void testFormatHql1() {
		Session session = sessionFactory.getCurrentSession();
		String s = "[["
				+ "],[]]";
			//	+ "{property:'age',condition:' not in ',listValue:[16,17,18]},"
			//	+ "{property:'age',condition:' between ',betweenValue:[18,23]},"
			//	+ "{property:'age',condition:' not between ',betweenValue:[18,23]}]]";
		List<User> list = (List<User>)new FormatHql(User.class,session,s).createQuery().list();
		for(User user:list){
			System.out.println(user.getAge());
		}
	}
	
	@Test
	public void testFormatHql2() {
		Session session = sessionFactory.getCurrentSession();
		String s = "[["
				+ "],[]]";
			//	+ "{property:'age',condition:' not in ',listValue:[16,17,18]},"
			//	+ "{property:'age',condition:' between ',betweenValue:[18,23]},"
			//	+ "{property:'age',condition:' not between ',betweenValue:[18,23]}]]";
		System.out.println(new FormatHql(User.class,session,s).getCount());
	}
	
	//@Test
	public void testPropertyUtils(){
		try {
			Class clazz = PropertyUtils.getPropertyType(User.class.newInstance(), "age");
			System.out.println(clazz.getName());	
		} catch (IllegalAccessException | InvocationTargetException
				| NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
