Ext.define("WebApp.test.store.UserStore",{
	extend:"WebApp.core.store.BaseStore",
	alias:"widget.UserStore",
	storeId:"UserStore",
	modelName:"User",
	actionUrl:"/webase/userAction!queryAll",
    autoLoad:true
})