Ext.define("WebApp.test.view.UserGrid", {
			extend : "WebApp.core.view.BaseGrid",
			requires : ["WebApp.core.ux.QueryPanel"],
			xtype : "UserGrid",
			columns : [{
						text : '姓名',
						dataIndex : 'name'
					}, {
						text : '年龄',
						dataIndex : 'age'
					}, {
						text : '邮箱',
						dataIndex : 'email',
						width : 200
					}, {
						text : '添加日期',
						dataIndex : 'addDate',
						flex : '2'
					}],
			autoRender : true,
			initComponent : function() {
				var me = this;
				me.store = Ext.create("WebApp.test.store.UserStore");
			
				this.callParent(arguments);
				var toolbar = this.getDockedItems('toolbar[dock="top"]')[0];
				console.log(toolbar);
				toolbar.add([{xtype : "QueryPanel",data : this.columns}]);
			}
		})