/**
 * 程序主控制器
 */
Ext.define("WebApp.core.controller.ButtonController",{
	extend:"Ext.app.Controller",
	initBtn:function(){
		var self=this;
		var btnCtr={
			/**
			 * 通用表格添加进表单
			 */
			"basegrid button[ref=gridInsertF]":{
					click:function(btn){
						console.log(Controller.Util.getParentC(btn).addView);
						Ext.create(Controller.Util.getParentC(btn).addView).show();
					}
			},
			/**
			 * 通用表格添加事件
			 */
			"basegrid button[ref=gridInsert]":{
					click:function(btn){
						console.log(btn.ref);
					}
			},
			/**
			 * 通用表格编辑事件
			 */
			"basegrid button[ref=gridEdit]":{
					click:function(btn){
						console.log(btn.ref);
					}
			},
			/**
			 *  通用表格删除事件
			 */
			"basegrid button[ref=gridDelete]":{
					click:function(btn){
						console.log(btn.ref);
					}
			},
			/**
			 * 通用表格保存事件
			 */
			"basegrid button[ref=gridSave]":{
					click:function(btn){
						console.log(btn.ref);
					}
			},
			/**
			 * 表单的保存
			 */
			"baseform button[ref=formSave]":{
					click:function(btn){
						console.log(btn.ref);
					}
			},
			/**
			 *  通用表单返回事件
			 */
			"baseform button[ref=formReturn]":{
					click:function(btn){
						console.log(btn.ref);
					}
			}
		}
		Ext.apply(self.ctr,btnCtr);
	}
});