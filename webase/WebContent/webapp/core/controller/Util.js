Ext.define("Controller.Util",{
	statics:{
		/**
		 * 获取顶层容器，目前支持basepanel basewindow
		 * @param {} target 当前的容器
		 * @return {}
		 */
		getParentC:function(target){
			var panel =target.up("basepanel");
			if(panel){
				return panel;
			} else {
				return target.up("basewindow");
			}
		}
	}
})