Ext.define("WebApp.core.store.BaseStore", {
			extend:"Ext.data.Store",
			remoteSort : true,
			constructor: function(config) {
				var me = this;
				me.fields = Factory.ModelFactory.getFields({modelName:me.modelName});
				me.proxy = {
						type : 'ajax',
						url : me.actionUrl,
						reader : {
							type : 'json'
						}
					}
		        this.callParent([config]);
		    }
		})