/**
 * 查询模块 调用方式 add([{xtype : "QueryPanel",data : this.columns}])
 * 
 * 组合查询样式如下
 * [{property:'name',condition:' is null '},
 *  {property:'name',condition:' is not null '},
 *  {property:'name',condition:' = ', singleValue:'zhou'},
 *  {property:'age',condition:' in ',listValue:[16,17,18]},
 *  {property:'age',condition:' not in ',listValue:[16,17,18]},
 *  {property:'age',condition:' between ',betweenValue:[18,23]},
 *  {property:'age',condition:' not between ',betweenValue:[18,23]}]
 * 
 * 	另一种方式	：
 * {
							withoutValue : [{property:'name',condition:' is null '},
											{property:'name',condition:' is not null '}	
											],
							singleValue:[{property:'name',condition:' = ',value:'zhou'},
										{property:'age',condition:' > ',value:25}	
										],
							listValue:[{property:'name',condition:' in ',value:['zhou','zhou1']},
										{property:'age',condition:' not in ',value:['zhou','zhou2']}	
										],
							betweenValue:[{property:'age',condition:' between ',value:[25,27]},
										{property:'age',condition:' not between ',value:[25,28]}	
										]
			}

 * 
 */
Ext.define("WebApp.core.ux.QueryPanel", {
			extend : "Ext.form.Panel",
			requires : ["WebApp.core.view.form.QueryComboBox",
					"WebApp.core.view.form.QueryConditionComboBox",
					"WebApp.core.view.form.QueryValueField"],
			xtype : "QueryPanel",
			height : 25,
			width : 510,
			autoRender : true,
			border : 0,
			data : null,
			layout : 'hbox',

			initComponent : function() {
				var me = this;

				Ext.apply(this, {
							items : [{
										xtype : "QueryComboBox",
										width :100,
										data : me.data
									}, {
										xtype : 'QueryConditionComboBox',
										width : 100
									}, {
										xtype : 'QueryValueField',
										type : 2,
										width : 100
									},{
										xtype:'button',
										text:'查询',
										handler:function(){
											console.log(me.getQueryObj());
										}
									}]
						})
				this.callParent(arguments);

			},
			
			/**
			 * 单一条件查询
			 */
			getQueryObj : function() {
				var valueField = this.down('QueryValueField');
				var property = this.down('QueryComboBox').getValue();
				var condition = this.down('QueryConditionComboBox').getValue();
				var value = valueField.getValue();
				value.property = property;
				value.condition = condition;
				return value;
				
			}
		
		})