/**
 * 组件的顶层容器
 */
Ext.define("WebApp.core.view.BasePanel",{
	extend : 'Ext.panel.Panel',
	alias : 'widget.basepanel',
	layout:"fit"
});