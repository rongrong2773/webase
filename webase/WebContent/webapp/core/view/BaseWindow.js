/**
 * 组件的顶层容器
 */
Ext.define("WebApp.core.view.BaseWindow",{
	extend : 'Ext.window.Window',
	alias : 'widget.basewindow',
	layout:"fit"
});