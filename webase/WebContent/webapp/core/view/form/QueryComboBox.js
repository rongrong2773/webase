Ext.define("WebApp.core.view.form.QueryComboBox", {
			extend : "Ext.form.field.ComboBox",
			xtype : "QueryComboBox",
			//fieldLabel : "选择属性",
			//labelWidth : 60,
			queryMode : 'local',
			displayField : 'text',
			valueField : 'dataIndex',
			autoRender : true,
			data : null,
			
			initComponent : function() {
				this.store = Ext.create("Ext.data.Store", {
							fields : ["text", "dataIndex"],
							data : this.data
						});
				this.addListener({
							afterRender : function(combo) {//设置默认值
								var firstValue = this.store.getAt(0);
								combo.setValue(firstValue);
							}
						});
				this.callParent(arguments);
			}

		})