/**
 * 查询条件下拉列表，内置部分查询条件（以后可以考虑放到服务器端）
 * 根据选择的查询条件可以改变查询范围的文本域样式
 */
Ext.define("WebApp.core.view.form.QueryConditionComboBox", {
			extend : "Ext.form.field.ComboBox",
			xtype : "QueryConditionComboBox",
			queryMode : 'local',
			displayField : 'name',
			valueField : 'value',
			parentContainer:"QueryPanel",
			autoRender:true,
			data : null,
			WithoutValue:1,
			SingleValue:2,
			ListValue:3,
			BetweenValue:4,
			hideLabel:true,
			initComponent : function() {
				this.store = Ext.create("Ext.data.Store", {
							fields : ["name", "value","type"],
							data : [
									{name:"为空",value:" is null ",type:this.WithoutValue},
									{name:"不为空",value:" is not null ",type:this.WithoutValue},
									{name:"等于",value:" = ",type:this.SingleValue},
									{name:"不等于",value:" <> ",type:this.SingleValue},
									{name:"大于",value:" > ",type:this.SingleValue},
									{name:"大于等于",value:" >= ",type:this.SingleValue},
									{name:"小于",value:" < ",type:this.SingleValue},
									{name:"小于等于",value:" <= ",type:this.SingleValue},
									{name:"含有",value:" like ",type:this.SingleValue},
									{name:"不含有",value:" not like ",type:this.SingleValue},
									{name:"包含在",value:" in ",type:this.ListValue},
									{name:"区间",value:" between ",type:this.BetweenValue},
									{name:"不在区间",value:" not between ",type:this.BetweenValue}
								]
						});
					this.addListener({
							afterRender : function(combo) {//设置默认值
								var firstValue = this.store.getAt(2);
								combo.setValue(firstValue);
							},
							
							//查询条件改变时修改查询值的文本域
							change:function(combo, newValue, oldValue, eOpts){
								var queryPanel = this.up(this.parentContainer);
								var valueFields = queryPanel.down("QueryValueField");
								switch(combo.findRecordByValue(newValue).data.type){
									
									case this.WithoutValue:
										console.log(1);
										if(valueFields.type != this.WithoutValue){
											queryPanel.remove(valueFields);
											queryPanel.add({xtype:'QueryValueField',type:1});
										}
										break;
										
									case this.SingleValue:
										if(valueFields.type != this.SingleValue){
											//Ext.Array.replace(items,2,1,[Ext.ComponentManager.create({xtype:'QueryValueField',type:1})]);
											queryPanel.remove(valueFields);
											queryPanel.add({xtype:'QueryValueField',type:2});
										}
										console.log(this.up(this.parentContainer).items.items);
										break;
										
									case this.ListValue:
										if(valueFields.type != this.ListValue){
											queryPanel.remove(valueFields);
											queryPanel.add({xtype:'QueryValueField',type:3});
										}
										break;
									case this.BetweenValue:
										if(valueFields.type != this.BetweenValue){
											queryPanel.remove(valueFields);
											queryPanel.add({xtype:'QueryValueField',type:4});
										}
										break;
								}
								//queryPanel.add({xtype:'button',text:"查询"});
								this.up(this.parentContainer).doLayout();
							}
						});
					this.callParent(arguments);
			}
		})