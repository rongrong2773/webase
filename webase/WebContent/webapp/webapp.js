Ext.QuickTips.init();
//初始化提示工具框
Ext.tip.QuickTipManager.init();
//启动动态加载机制
Ext.Loader.setConfig({
	enabled:true,
	paths:{
		Factory:"webapp/core/util/factory",
		Controller:"webapp/core/controller"
	}
})
//同步加载
Ext.syncRequire([
        "Factory.ModelFactory",
        "Factory.DDCache",
        "Controller.Util"
]);

Ext.application({
	    name: 'WebApp',
	    appFolder:"webapp",
	    launch:function(){
	    	console.log("主程序入口启动");
	    	Ext.create("WebApp.test.view.Test").show();
	    },
	    controllers:[
         "WebApp.core.controller.MainController",//装在核心控制器
       	 "WebApp.test.controller.TestController"
        ]
	
});


