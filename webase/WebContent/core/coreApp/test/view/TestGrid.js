Ext.define("core.test.view.TestGrid",{
	extend:"core.app.base.BaseGrid",
	alias:"widget.testgrid",
	columns:[{
		xtype:"rownumberer",
		width : 35,
		text :'No.',
		align : 'center'
	},{
		text:"名称",
		dataIndex:"name",
		field:{
			xtype:"textfield"
		}
	},{
		text:"编码",
		dataIndex:"code",
		field:{
			xtype:"textfield"
		}
	},{
		text:"出生日期",
		dataIndex:"birthday",
		field:{
			xtype:"datetimefield",
			dateType:"date"
		}		
	}],
	store:Ext.create("Ext.data.Store",{
		fields:[
			{name:"name",type:"string"},
			{name:"code",type:"string"},
			{name:"birthday",type:"string"}
			]
	})
});